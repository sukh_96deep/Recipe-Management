/*
    Sukhmandeep Singh Bassi
    100275305
    Assingment#1 - CPSC 2261
**/
import {Fridge} from "./fridge.class"
import {Recipe} from "./recipe.class"
import {Ingredient} from "./ingredient.class";

describe("Fridge testing", function(){

    //add, remove, checkRecipe
    let fridge = new Fridge();
        let qty = 10;
    it("Add New Item", function(){
        
        fridge.add("Orange", qty);
        let tempItem = fridge.contents[0];
        for(var i =0; i<fridge.contents.length; i++){
            if(fridge.contents[i].name == "Orange"){
                 tempItem = fridge.contents[i];
            }
        }
       
        expect(tempItem.name).toBe("Orange");
        expect(tempItem.quantity).toBe(qty);
        
    });

    it("Adding same item again", function(){
        
        let tempItem = fridge.contents[0];
        for(var i =0; i < fridge.contents.length; i++){
            if(fridge.contents[i].name == "Orange"){
                 tempItem = fridge.contents[i];
            }
        }
        
        fridge.add("Orange", 5);
        console.log(tempItem);
        expect(tempItem.quantity).toBe(qty+5);

    });

    it("Removing Item", function(){
        
        let tempItem = fridge.contents[0];
        let oldQuantity = tempItem.quantity;
        fridge.remove("Orange", 1);
        expect(tempItem.quantity).toBe(oldQuantity -1);

    });   

    it("Checking Recipe", function(){
        let fridge = new Fridge();
        fridge.add("apple", 12);
        fridge.add("flour", 4);
        fridge.add("butter", 5);
        
        
        let itemsToBeAdded: Array <Ingredient> = new Array<Ingredient>();
        itemsToBeAdded.push(new Ingredient("boh", 20));
        itemsToBeAdded.push(new Ingredient("new", 20));
        itemsToBeAdded.push(new Ingredient("apple", 13));
        itemsToBeAdded.push(new Ingredient("flour", 5));
        itemsToBeAdded.push(new Ingredient("butter", 2));

        let recipeEx = new Recipe();
        for(let i of itemsToBeAdded){
            recipeEx.addItem(i);
        }
        let originalList: string [][] = [["boh", "new", "apple", "flour"], ["flour", "butter"]];
        let returnlist: string [][] = fridge.checkrecipe(recipeEx);
        for(let i=0; i<returnlist.length; i++){
            for(let j =0; j<i; j++){
                expect(returnlist[i]).toContain(originalList[i][j]);
            }
        }

    });

});

/*
    Sukhmandeep Singh Bassi
    100275305
    Assingment#1 - CPSC 2261
**/
export class Ingredient{
    constructor(public name: string, public quantity: number){
    }

    add(amount: number){
        this.quantity += amount;
    }
    subtract(amount: number){  
        this.quantity -= amount;
    }
}